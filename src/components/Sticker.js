import React, { Component } from "react";
import { isIOS } from "react-device-detect";

import Asset1 from "../assets/img/stickers/01-Witch-01.png";
import Asset2 from "../assets/img/stickers/01-Witch-02.png";
import Asset3 from "../assets/img/stickers/01-Witch-03.png";
import Asset4 from "../assets/img/stickers/02-Frankenstein-01.png";
import Asset5 from "../assets/img/stickers/02-Frankenstein-02.png";
import Asset6 from "../assets/img/stickers/02-Frankenstein-03.png";
import Asset7 from "../assets/img/stickers/03_Mummy-01.png";
import Asset8 from "../assets/img/stickers/03_Mummy-02.png";
import Asset9 from "../assets/img/stickers/03_Mummy-03.png";
import Asset10 from "../assets/img/stickers/04-vampire-01.png";
import Asset11 from "../assets/img/stickers/04-vampire-02.png";
import Asset12 from "../assets/img/stickers/04-vampire-03.png";
import Asset13 from "../assets/img/stickers/05-boy-01.png";
import Asset14 from "../assets/img/stickers/05-boy-02.png";
import Asset15 from "../assets/img/stickers/05-boy-03.png";
import Asset16 from "../assets/img/stickers/06-girl-01.png";
import Asset17 from "../assets/img/stickers/06-girl-02.png";
import Asset18 from "../assets/img/stickers/06-girl-03.png";
import Asset19 from "../assets/img/stickers/07-cat-01.png";
import Asset20 from "../assets/img/stickers/07-cat-02.png";
import Asset21 from "../assets/img/stickers/07-cat-03.png";
import Asset22 from "../assets/img/stickers/08-man-01.png";
import Asset23 from "../assets/img/stickers/08-man-02.png";
import Asset24 from "../assets/img/stickers/08-man-03.png";
import stickerMaker from "../assets/icon/sticker-maker.png";
import stickerly from "../assets/icon/stickerly.png";
import whatSticker from "../assets/icon/whatsticker.png";
import 五色學倉頡 from "../assets/icon/五色學倉頡.png";

export class DownloadStickers extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isSaveToWhatsapp: false,
      stickers: {
        witch: [Asset1, Asset2, Asset3],
        frank: [Asset4, Asset5, Asset6],
        mummy: [Asset7, Asset8, Asset9],
        vampire: [Asset10, Asset11, Asset12],
        boy: [Asset13, Asset14, Asset15],
        girl: [Asset16, Asset17, Asset18],
        cat: [Asset19, Asset20, Asset21],
        pumpkin: [Asset22, Asset23, Asset24],
      },
      appLinks: {
        ios: {
          witch: [
            "fivecj://348225uwofIHr",
            "https://loadstickerpack.page.link/?link=https://getstickerpack.com/stickers/devb-7/launch-app&apn=com.marsvard.stickermakerforwhatsapp&amv=71&isi=1443326857&ibi=com.tomatamara.stickermaker&ipbi=com.tomatamara.stickermaker&st=DEVB&sd=Download+DEVB+by+kkevinhb&si=https://s3.getstickerpack.com/storage/uploads/sticker-pack/devb-7/tray_large.png&cid=2697595303747969401",
            "https://stickerly.onelink.me/wfhv?pid=stickerly_web&is_retargeting=true&af_web_dp=http%3A%2F%2Fsticker.ly&af_ios_fallback=http%3A%2F%2Fsticker.ly&af_dp=stickerly%3A%2F%2Fdownload%3FpackId%3D5VM18X",
          ],
          frank: [
            "fivecj://348223LhHM42p",
            "https://loadstickerpack.page.link/?link=https://getstickerpack.com/stickers/devb-6/launch-app&amp;apn=com.marsvard.stickermakerforwhatsapp&amp;amv=71&amp;isi=1443326857&amp;ibi=com.tomatamara.stickermaker&amp;ipbi=com.tomatamara.stickermaker&amp;st=DEVB&amp;sd=Download+DEVB+by+kkevinhb&amp;si=https://s3.getstickerpack.com/storage/uploads/sticker-pack/devb-6/tray_large.png&amp;cid=6163916511163269957",
            "https://stickerly.onelink.me/wfhv?pid=stickerly_web&is_retargeting=true&af_web_dp=http%3A%2F%2Fsticker.ly&af_ios_fallback=http%3A%2F%2Fsticker.ly&af_dp=stickerly%3A%2F%2Fdownload%3FpackId%3D8YRMDG",
          ],
          mummy: [
            "fivecj://348221CA96C76",
            "https://loadstickerpack.page.link/?link=https://getstickerpack.com/stickers/devb-5/launch-app&amp;apn=com.marsvard.stickermakerforwhatsapp&amp;amv=71&amp;isi=1443326857&amp;ibi=com.tomatamara.stickermaker&amp;ipbi=com.tomatamara.stickermaker&amp;st=DEVB&amp;sd=Download+DEVB+by+kkevinhb&amp;si=https://s3.getstickerpack.com/storage/uploads/sticker-pack/devb-5/tray_large.png&amp;cid=8403928275507878208",
            "https://stickerly.onelink.me/wfhv?pid=stickerly_web&is_retargeting=true&af_web_dp=http%3A%2F%2Fsticker.ly&af_ios_fallback=http%3A%2F%2Fsticker.ly&af_dp=stickerly%3A%2F%2Fdownload%3FpackId%3D6RVEWX",
          ],
          vampire: [
            "fivecj://348219J6M7uVJ",
            "https://loadstickerpack.page.link/?link=https://getstickerpack.com/stickers/devb-4/launch-app&amp;apn=com.marsvard.stickermakerforwhatsapp&amp;amv=71&amp;isi=1443326857&amp;ibi=com.tomatamara.stickermaker&amp;ipbi=com.tomatamara.stickermaker&amp;st=DEVB&amp;sd=Download+DEVB+by+kkevinhb&amp;si=https://s3.getstickerpack.com/storage/uploads/sticker-pack/devb-4/tray_large.png&amp;cid=574477706576562611",
            "https://stickerly.onelink.me/wfhv?pid=stickerly_web&is_retargeting=true&af_web_dp=http%3A%2F%2Fsticker.ly&af_ios_fallback=http%3A%2F%2Fsticker.ly&af_dp=stickerly%3A%2F%2Fdownload%3FpackId%3DG6O7G7",
          ],
          boy: [
            "fivecj://348217r0SZTej",
            "https://loadstickerpack.page.link/?link=https://getstickerpack.com/stickers/devb-3/launch-app&amp;apn=com.marsvard.stickermakerforwhatsapp&amp;amv=71&amp;isi=1443326857&amp;ibi=com.tomatamara.stickermaker&amp;ipbi=com.tomatamara.stickermaker&amp;st=DEVB&amp;sd=Download+DEVB+by+kkevinhb&amp;si=https://s3.getstickerpack.com/storage/uploads/sticker-pack/devb-3/tray_large.png&amp;cid=7045264452913458737",
            "https://stickerly.onelink.me/wfhv?pid=stickerly_web&is_retargeting=true&af_web_dp=http%3A%2F%2Fsticker.ly&af_ios_fallback=http%3A%2F%2Fsticker.ly&af_dp=stickerly%3A%2F%2Fdownload%3FpackId%3D8U9RNO",
          ],
          girl: [
            "fivecj://348215Uy9UugN",
            "https://loadstickerpack.page.link/?link=https://getstickerpack.com/stickers/devb-2/launch-app&amp;apn=com.marsvard.stickermakerforwhatsapp&amp;amv=71&amp;isi=1443326857&amp;ibi=com.tomatamara.stickermaker&amp;ipbi=com.tomatamara.stickermaker&amp;st=DEVB&amp;sd=Download+DEVB+by+kkevinhb&amp;si=https://s3.getstickerpack.com/storage/uploads/sticker-pack/devb-2/tray_large.png&amp;cid=2781596838679522347",
            "https://stickerly.onelink.me/wfhv?pid=stickerly_web&is_retargeting=true&af_web_dp=http%3A%2F%2Fsticker.ly&af_ios_fallback=http%3A%2F%2Fsticker.ly&af_dp=stickerly%3A%2F%2Fdownload%3FpackId%3DED0ZR9",
          ],
          cat: [
            "fivecj://348195Md5rhAJ",
            "https://loadstickerpack.page.link/?link=https://getstickerpack.com/stickers/devb-1/launch-app&amp;apn=com.marsvard.stickermakerforwhatsapp&amp;amv=71&amp;isi=1443326857&amp;ibi=com.tomatamara.stickermaker&amp;ipbi=com.tomatamara.stickermaker&amp;st=DEVB&amp;sd=Download+DEVB+by+kkevinhb&amp;si=https://s3.getstickerpack.com/storage/uploads/sticker-pack/devb-1/tray_large.png&amp;cid=3915332457473791419",
            "https://stickerly.onelink.me/wfhv?pid=stickerly_web&is_retargeting=true&af_web_dp=http%3A%2F%2Fsticker.ly&af_ios_fallback=http%3A%2F%2Fsticker.ly&af_dp=stickerly%3A%2F%2Fdownload%3FpackId%3DLBVEC7",
          ],
          pumpkin: [
            "fivecj://348193wLDA6LU",
            "https://loadstickerpack.page.link/?link=https://getstickerpack.com/stickers/devb/launch-app&amp;apn=com.marsvard.stickermakerforwhatsapp&amp;amv=71&amp;isi=1443326857&amp;ibi=com.tomatamara.stickermaker&amp;ipbi=com.tomatamara.stickermaker&amp;st=DEVB&amp;sd=Download+DEVB+by+kkevinhb&amp;si=https://s3.getstickerpack.com/storage/uploads/sticker-pack/devb/tray_large.png&amp;cid=4669325853758863748",
            "https://stickerly.onelink.me/wfhv?pid=stickerly_web&is_retargeting=true&af_web_dp=http%3A%2F%2Fsticker.ly&af_ios_fallback=http%3A%2F%2Fsticker.ly&af_dp=stickerly%3A%2F%2Fdownload%3FpackId%3DX7SRHV",
          ],
        },
        android: {
          witch: [
            "https://whatsticker.online/cache2/dl/348225uwofIHr/android",
            "https://loadstickerpack.page.link/?link=https://getstickerpack.com/stickers/devb-7/launch-app&apn=com.marsvard.stickermakerforwhatsapp&amv=71&isi=1443326857&ibi=com.tomatamara.stickermaker&ipbi=com.tomatamara.stickermaker&st=DEVB&sd=Download+DEVB+by+kkevinhb&si=https://s3.getstickerpack.com/storage/uploads/sticker-pack/devb-7/tray_large.png&cid=2697595303747969401",
            "https://stickerly.onelink.me/wfhv?pid=stickerly_web&is_retargeting=true&af_web_dp=http%3A%2F%2Fsticker.ly&af_ios_fallback=http%3A%2F%2Fsticker.ly&af_dp=stickerly%3A%2F%2Fdownload%3FpackId%3D5VM18X",
          ],
          frank: [
            "https://whatsticker.online/cache2/dl/348223LhHM42p/android",
            "https://loadstickerpack.page.link/?link=https://getstickerpack.com/stickers/devb-6/launch-app&amp;apn=com.marsvard.stickermakerforwhatsapp&amp;amv=71&amp;isi=1443326857&amp;ibi=com.tomatamara.stickermaker&amp;ipbi=com.tomatamara.stickermaker&amp;st=DEVB&amp;sd=Download+DEVB+by+kkevinhb&amp;si=https://s3.getstickerpack.com/storage/uploads/sticker-pack/devb-6/tray_large.png&amp;cid=6163916511163269957",
            "https://stickerly.onelink.me/wfhv?pid=stickerly_web&is_retargeting=true&af_web_dp=http%3A%2F%2Fsticker.ly&af_ios_fallback=http%3A%2F%2Fsticker.ly&af_dp=stickerly%3A%2F%2Fdownload%3FpackId%3D8YRMDG",
          ],
          mummy: [
            "https://whatsticker.online/cache2/dl/348221CA96C76/android",
            "https://loadstickerpack.page.link/?link=https://getstickerpack.com/stickers/devb-5/launch-app&amp;apn=com.marsvard.stickermakerforwhatsapp&amp;amv=71&amp;isi=1443326857&amp;ibi=com.tomatamara.stickermaker&amp;ipbi=com.tomatamara.stickermaker&amp;st=DEVB&amp;sd=Download+DEVB+by+kkevinhb&amp;si=https://s3.getstickerpack.com/storage/uploads/sticker-pack/devb-5/tray_large.png&amp;cid=8403928275507878208",
            "https://stickerly.onelink.me/wfhv?pid=stickerly_web&is_retargeting=true&af_web_dp=http%3A%2F%2Fsticker.ly&af_ios_fallback=http%3A%2F%2Fsticker.ly&af_dp=stickerly%3A%2F%2Fdownload%3FpackId%3D6RVEWX",
          ],
          vampire: [
            "https://whatsticker.online/cache2/dl/348219J6M7uVJ/android",
            "https://loadstickerpack.page.link/?link=https://getstickerpack.com/stickers/devb-4/launch-app&amp;apn=com.marsvard.stickermakerforwhatsapp&amp;amv=71&amp;isi=1443326857&amp;ibi=com.tomatamara.stickermaker&amp;ipbi=com.tomatamara.stickermaker&amp;st=DEVB&amp;sd=Download+DEVB+by+kkevinhb&amp;si=https://s3.getstickerpack.com/storage/uploads/sticker-pack/devb-4/tray_large.png&amp;cid=574477706576562611",
            "https://stickerly.onelink.me/wfhv?pid=stickerly_web&is_retargeting=true&af_web_dp=http%3A%2F%2Fsticker.ly&af_ios_fallback=http%3A%2F%2Fsticker.ly&af_dp=stickerly%3A%2F%2Fdownload%3FpackId%3DG6O7G7",
          ],
          boy: [
            "https://whatsticker.online/cache2/dl/348217r0SZTej/android",
            "https://loadstickerpack.page.link/?link=https://getstickerpack.com/stickers/devb-3/launch-app&amp;apn=com.marsvard.stickermakerforwhatsapp&amp;amv=71&amp;isi=1443326857&amp;ibi=com.tomatamara.stickermaker&amp;ipbi=com.tomatamara.stickermaker&amp;st=DEVB&amp;sd=Download+DEVB+by+kkevinhb&amp;si=https://s3.getstickerpack.com/storage/uploads/sticker-pack/devb-3/tray_large.png&amp;cid=7045264452913458737",
            "https://stickerly.onelink.me/wfhv?pid=stickerly_web&is_retargeting=true&af_web_dp=http%3A%2F%2Fsticker.ly&af_ios_fallback=http%3A%2F%2Fsticker.ly&af_dp=stickerly%3A%2F%2Fdownload%3FpackId%3D8U9RNO",
          ],
          girl: [
            "https://whatsticker.online/cache2/dl/348215Uy9UugN/android",
            "https://loadstickerpack.page.link/?link=https://getstickerpack.com/stickers/devb-2/launch-app&amp;apn=com.marsvard.stickermakerforwhatsapp&amp;amv=71&amp;isi=1443326857&amp;ibi=com.tomatamara.stickermaker&amp;ipbi=com.tomatamara.stickermaker&amp;st=DEVB&amp;sd=Download+DEVB+by+kkevinhb&amp;si=https://s3.getstickerpack.com/storage/uploads/sticker-pack/devb-2/tray_large.png&amp;cid=2781596838679522347",
            "https://stickerly.onelink.me/wfhv?pid=stickerly_web&is_retargeting=true&af_web_dp=http%3A%2F%2Fsticker.ly&af_ios_fallback=http%3A%2F%2Fsticker.ly&af_dp=stickerly%3A%2F%2Fdownload%3FpackId%3DED0ZR9",
          ],
          cat: [
            "https://whatsticker.online/cache2/dl/348195Md5rhAJ/android",
            "https://loadstickerpack.page.link/?link=https://getstickerpack.com/stickers/devb-1/launch-app&amp;apn=com.marsvard.stickermakerforwhatsapp&amp;amv=71&amp;isi=1443326857&amp;ibi=com.tomatamara.stickermaker&amp;ipbi=com.tomatamara.stickermaker&amp;st=DEVB&amp;sd=Download+DEVB+by+kkevinhb&amp;si=https://s3.getstickerpack.com/storage/uploads/sticker-pack/devb-1/tray_large.png&amp;cid=3915332457473791419",
            "https://stickerly.onelink.me/wfhv?pid=stickerly_web&is_retargeting=true&af_web_dp=http%3A%2F%2Fsticker.ly&af_ios_fallback=http%3A%2F%2Fsticker.ly&af_dp=stickerly%3A%2F%2Fdownload%3FpackId%3DLBVEC7",
          ],
          pumpkin: [
            "https://whatsticker.online/cache2/dl/348193wLDA6LU/android",
            "https://loadstickerpack.page.link/?link=https://getstickerpack.com/stickers/devb/launch-app&amp;apn=com.marsvard.stickermakerforwhatsapp&amp;amv=71&amp;isi=1443326857&amp;ibi=com.tomatamara.stickermaker&amp;ipbi=com.tomatamara.stickermaker&amp;st=DEVB&amp;sd=Download+DEVB+by+kkevinhb&amp;si=https://s3.getstickerpack.com/storage/uploads/sticker-pack/devb/tray_large.png&amp;cid=4669325853758863748",
            "https://stickerly.onelink.me/wfhv?pid=stickerly_web&is_retargeting=true&af_web_dp=http%3A%2F%2Fsticker.ly&af_ios_fallback=http%3A%2F%2Fsticker.ly&af_dp=stickerly%3A%2F%2Fdownload%3FpackId%3DX7SRHV",
          ],
        },
      },
      appSrc: {
        ios: [五色學倉頡, stickerMaker, stickerly],
        android: [whatSticker, stickerMaker, stickerly],
      },
      appText: {
        ios: ["五色學倉頡 Online", "Sticker Maker Studio", "Sticker.ly"],
        android: ["Whatsticker", "Sticker Maker Studio", "Sticker.ly"],
      },
    };
  }

  handleClickSave = () => {
    this.setState({
      isSaveToWhatsapp: true,
    });
  };

  handleOnClick = (link) => {
    if (link.startsWith("fivecj://")) {
      let timeout;
      const preventPopup = () => {
        clearTimeout(timeout);
        timeout = null;
        window.removeEventListener("pagehide", preventPopup);
      };
      const startApp = () => {
        timeout = setTimeout(function () {
          window.location =
            "https://apps.apple.com/hk/app/%E4%BA%94%E8%89%B2%E5%AD%B8%E5%80%89%E9%A0%A1-online/id382024751";
        }, 3000);
        window.location = link;
        window.addEventListener("pagehide", preventPopup);
      };
      startApp();
    } else {
      window.location = link;
    }
  };

  render() {
    const {
      stickers,
      isSaveToWhatsapp,
      appLinks,
      appSrc,
      appText,
    } = this.state;
    const { character } = this.props;

    let content = null;

    if (!isSaveToWhatsapp) {
      content = (
        <div>
          <div className="header-container">
            <div>恭喜你已成功獲得嘩鬼WhatsApp貼圖！</div>
            <div>Congratulation! You have got the Halloween stickers</div>
          </div>
          <div className="sticker-images-container">
            {stickers[character].map((sticker) => (
              <img className="sticker-images" src={sticker} key={sticker} />
            ))}
          </div>
          <div className="sticker-btn-container">
            <button className="btn-green" onClick={this.handleClickSave}>
              <div>儲存到Whatsapp</div>
              <div>Save to Whatsapp</div>
            </button>
            <button
              onClick={this.props.backToImageTracking}
              className="btn-gray"
            >
              <div>取消</div>
              <div>cancel</div>
            </button>
          </div>
        </div>
      );
    } else {
      content = (
        <div className="sticker-container-save">
          <div
            onClick={this.props.backToImageTracking}
            className="close-preview-button"
          >
            X
          </div>
          <div className="header-container">
            <div>請選擇或安裝以下應用程式儲存</div>
            <div>Select or install on of the below Apps for the download.</div>
          </div>
          <div className="sticker-images-container">
            {stickers[character].map((sticker) => (
              <img className="sticker-images" src={sticker} key={sticker} />
            ))}
          </div>
          <div className="sticker-footer">
            <div className="sticker-footer-container">
              {appLinks[`${isIOS ? "ios" : "android"}`][character].map(
                (link, index) => (
                  <div className="sticker-footer-images-container" key={index}>
                    <a onClick={() => this.handleOnClick(link)}>
                      <img
                        id={
                          (isIOS ? "ios" : "android") + "-icon-" + (index + 1)
                        }
                        className={
                          "sticker-footer-images " +
                          (isIOS ? "ios" : "android") +
                          "-icons"
                        }
                        alt={"app icon " + (index + 1)}
                        src={appSrc[isIOS ? "ios" : "android"][index]}
                      />
                      <div>{appText[isIOS ? "ios" : "android"][index]}</div>
                    </a>
                  </div>
                )
              )}
            </div>
          </div>
        </div>
      );
    }

    return <div className="sticker-container">{content}</div>;
  }
}

export default DownloadStickers;
