// import "aframe";
import React, { Component } from "react";
import "../assets/css/App.css";
import softEyesImage from "../assets/img/face-textures/soft-eyes-mouth.png";
import witchFace from "../assets/img/face-textures/WITCHFACE.png";

import occluder from "../assets/models/head-occluder.glb";
import witchHat from "../assets/models/HATOCT07FINAL.glb";

class FaceFilter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isCapturing: true,
      isUploading: false,
      imageURL: null,
    };
    this.myRef = React.createRef();
  }

  componentDidMount() {
    let that = this;
    console.log(this.myRef);
    window.addEventListener("mediarecorder-photocomplete", function (event) {
      that.setState({ isUploading: true, isCapturing: false }, () => {
        that.uploadImage(event.detail.blob);
      });
    });
  }

  uploadImage = async (blob) => {
    let s3URL = await fetch(
      "https://ztjyg3beya.execute-api.ap-east-1.amazonaws.com/dev/presigned_url",
      {
        // Your POST endpoint
        method: "POST",
      }
    );

    s3URL = await s3URL.json();

    console.log(s3URL);

    let uploadResult = await fetch(s3URL.url, {
      // Your POST endpoint
      method: "PUT",
      headers: {
        // Content-Type may need to be completely **omitted**
        // or you may need something
        "Content-Type": "image/jpeg",
      },
      body: blob, // This is your file object
    });

    this.setState({
      isUploading: false,
      imageURL: `https://devb-upload.s3.ap-east-1.amazonaws.com/${s3URL.filename}`,
    });

    console.log(uploadResult);
  };

  closePreview = () => {
    this.setState({
      isCapturing: true,
    });
  };

  render() {
    const { isCapturing, imageURL } = this.state;
    return (
      <div>
        <a-scene
          id="face-filter-scene"
          embedded
          xrextras-almost-there
          xrextras-loading
          xrextras-runtime-error
          renderer="maxCanvasWidth: 960; maxCanvasHeight: 960"
          xrface="mirroredDisplay: true;  meshGeometry: eyes, face, mouth; cameraDirection: front; allowedDevices: any;"
        >
          <xrextras-resource
            id="witch-face"
            src={witchFace}
          ></xrextras-resource>
          <xrextras-resource id="alpha" src={softEyesImage}></xrextras-resource>
          <xrextras-basic-material
            id="face-texture"
            tex="#witch-face"
            alpha="#alpha"
            opacity="0.9"
          ></xrextras-basic-material>

          <xrextras-capture-button ref={this.myRef} capture-mode="photo">
            {" "}
          </xrextras-capture-button>
          {/* <xrextras-capture-preview></xrextras-capture-preview> */}

          <a-camera position="0 1 2"> </a-camera>

          <xrextras-faceanchor>
            <a-entity
              id="occluder-entity"
              gltf-model={occluder}
              position="0 0 0.02"
              xrextras-hider-material
            ></a-entity>

            <xrextras-face-mesh
              scale="1 1 1"
              position="0 0 0"
              material-resource="#face-texture"
            ></xrextras-face-mesh>

            <xrextras-face-attachment point="forehead">
              <a-entity
                id="witch-hat-entity"
                gltf-model={witchHat}
                scale="0.8 0.8 0.8"
                rotation="100 180 0"
                position="0 -0.2 0.5"
              ></a-entity>
            </xrextras-face-attachment>
          </xrextras-faceanchor>

          <a-light
            type="directional"
            target="#face"
            position="0 2.2 3"
            intensity="1"
          ></a-light>
        </a-scene>

        {!isCapturing ? (
          <div>
            <img className="preview" src={imageURL} />
            <div onClick={this.closePreview} className="close-preview-button">
              X
            </div>
            <div className="footer">
            長按相片收藏並與家人朋友分享吧！
              <br />
              Long press the photo to save and share with your family and friends!
            </div>
          </div>
          ) : null}
      </div>
    );
  }
}

export default FaceFilter;
