import React, { Component, createRef } from "react";
import * as tmImage from "@teachablemachine/image";

class TeachableMachineTracking extends Component {
  constructor(props) {
    super(props);
    this.videoRef = createRef();

    this.state = { predictions: null };

    switch (props.markerType) {
      case "marker1":
        this.state.detectClass = ["TV"];
        this.state.faceFilter = 1;
        break;
      case "marker2":
        this.state.detectClass = ["Frank", "Mummy", "Vampire", "Witch"];
        this.state.imageSticker = 1;
        break;
      case "marker3":
        this.state.detectClass = ["Sticker", "Bigtv"];
        this.state.faceFilter = 2;
        break;
      case "marker4":
        this.state.detectClass = ["TV"];
        this.state.faceFilter = 3;
        break;
      case "marker5":
        this.state.detectClass = ["Pumpkin", "Boy", "Girl", "Cat"];
        this.state.imageSticker = 2;
        break;
      case "marker6":
        this.state.detectClass = ["TV"];
        this.state.faceFilter = 4;
        break;
    }

    this.loop = this.loop.bind(this);
    this.predict = this.predict.bind(this);
  }

  async componentDidMount() {
    const URL = "https://storage.googleapis.com/tm-model/gh4SD1u8v/";
    const modelURL = URL + "model.json";
    const metadataURL = URL + "metadata.json";

    // load the model and metadata
    // Refer to tmImage.loadFromFiles() in the API to support files from a file picker
    // or files from your local hard drive
    this.model = await tmImage.load(modelURL, metadataURL);

    let webcamStream = await navigator.mediaDevices.getUserMedia({
      video: {
        advanced: [
          {
            facingMode: "environment",
          },
        ],
      },
      audio: false,
    });

    this.videoRef.current.srcObject = webcamStream;
    window.stream = webcamStream;
    // pass the stream to the videoRef
    const delay = (s) => {
      return new Promise((resolve) => {
        setTimeout(resolve, s);
      });
    };

    await delay(2500); //Delay 2.5s
    window.requestId = window.requestAnimationFrame(this.loop);
  }

  async loop() {
    try {
      await this.predict();
    } catch (err) {
      console.log(err);
    }
  }

  async predict() {
    const { detectClass } = this.state;
    const { triggerFaceFilter, triggerWASticker, markerType } = this.props;
    if (this.videoRef.current.srcObject !== null) {
      const prediction = await this.model.predict(this.videoRef.current);

      this.setState({ predictions: prediction });

      console.log(prediction);

      let probabilities = {
        Bigtv: 0.95,
        TV: 0.95,
        Sticker: 0.95,
        Frank: 0.9,
        Mummy: 1,
        Vampire: 0.9,
        Witch: 0.9,
        Pumpkin: 0.9,
        Boy: 0.9,
        Girl: 0.99,
        Cat: 0.9,
      };

      prediction.forEach((val) => {
        if (
          detectClass.includes(val.className) &&
          val.probability >= probabilities[val.className]
        ) {
          this.stop();
          if (
            val.className === "TV" ||
            val.className === "Sticker" ||
            val.className === "Bigtv"
          ) {
            triggerFaceFilter(markerType);
          } else if (
            [
              "Frank",
              "Mummy",
              "Vampire",
              "Witch",
              "Pumpkin",
              "Boy",
              "Girl",
              "Cat",
            ].includes(val.className)
          ) {
            triggerWASticker(val.className.toLowerCase());
          }
        }
      });

      window.requestAnimationFrame(this.loop);
    }
  }

  stop() {
    window.cancelAnimationFrame(this.requestId);
    this.requestId = null;
    var stream = this.videoRef.current.srcObject;
    var tracks = stream.getTracks();

    for (var i = 0; i < tracks.length; i++) {
      var track = tracks[i];
      track.stop();
    }

    this.videoRef.current.srcObject = null;
  }

  renderProbs() {
    const { predictions } = this.state;

    if (predictions === null) {
      return [];
    }

    let result = [];

    predictions.sort((a, b) => a.probability - b.probability);

    for (let i = 0; i < predictions.length; i++) {
      result.push(
        <p key={i}>
          {predictions[i].className}
          <br />
          {predictions[i].probability}
        </p>
      );
    }

    return result;
  }

  render() {
    const styles = {
      height: "100vh",
    };

    return (
      <div>
        <video
          muted
          ref={this.videoRef}
          autoPlay
          playsInline
          style={styles}
          controls={false}
        />
      </div>
    );
  }
}

export default TeachableMachineTracking;
