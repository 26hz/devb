import React, { Component } from "react";
import haversine from "haversine-distance";
import ReactGA from "react-ga";
import copy from 'copy-to-clipboard';
import { isChrome, isSafari, isAndroid, isIOS, isBrowser } from "react-device-detect";

import "./assets/css/App.css";

import FaceFilterWitch from "./components/FaceFilter";
import FaceFilterDevil from "./components/FaceFilter2";
import FaceFilterBat from "./components/FaceFilter3";
import FaceFilterPumpkin from "./components/FaceFilter4";
import WASticker from "./components/Sticker";

import topBanner from "./assets/img/top-banner.png";
import pumpkinIcon from "./assets/icon/pumpkin-icon.png";
import waIcon from "./assets/icon/witch-icon.png";
import pumpkinInfo from "./assets/img/pumpkin-info.png";
import frame from "./assets/img/camera-frame.png";
import safariLogo from "./assets/img/safari.png";
import chromeLogo from "./assets/img/chrome.png";
import TeachableMachineTracking from "./components/TeachableMachineTracking";

const style = {
  repositionUserToMarker1: {
    top: "11%",
    left: "72%",
    display: "block",
  },
  repositionUserToMarker2: {
    top: "40%",
    left: "79%",
    display: "block",
  },
  repositionUserToMarker3: {
    top: "43%",
    left: "75%",
    display: "block",
  },
  repositionUserToMarker4: {
    top: "49%",
    left: "62%",
    display: "block",
  },
  repositionUserToMarker5: {
    top: "63%",
    left: "42%",
    display: "block",
  },
  repositionUserToMarker6: {
    top: "67%",
    left: "19%",
    display: "block",
  },
  repositionUserToMarker7: {
    top: "65.5%",
    left: "22%",
    display: "block",
  },
  repositionUserToMarker8: {
    top: "64%",
    left: "26%",
    display: "block",
  },
  repositionUserToMarker9: {
    top: "63%",
    left: "34%",
    display: "block",
  },
  repositionUserToMarker10: {
    top: "61%",
    left: "40%",
    display: "block",
  },
  repositionUserToMarker11: {
    top: "59%",
    left: "44%",
    display: "block",
  },
  repositionUserToMarker12: {
    top: "54%",
    left: "52%",
    display: "block",
  },
  repositionUserToMarker13: {
    top: "52%",
    left: "56%",
    display: "block",
  },
  repositionUserToMarker14: {
    top: "48%",
    left: "62%",
    display: "block",
  },
  repositionUserToMarker15: {
    top: "45%",
    left: "66%",
    display: "block",
  },
  repositionUserToMarker16: {
    top: "43%",
    left: "70%",
    display: "block",
  },
  repositionUserToMarker17: {
    top: "41%",
    left: "75%",
    display: "block",
  },
  repositionUserToMarker18: {
    top: "36%",
    left: "81%",
    display: "block",
  },
  repositionUserToMarker19: {
    top: "31%",
    left: "85%",
    display: "block",
  },
  repositionUserToMarker20: {
    top: "25%",
    left: "85%",
    display: "block",
  },
  repositionUserToMarker21: {
    top: "20%",
    left: "83%",
    display: "block",
  },
  repositionUserToMarker22: {
    top: "17%",
    left: "80%",
    display: "block",
  },
  repositionUserToMarker23: {
    top: "14%",
    left: "78%",
    display: "block",
  },
};

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      locationURL: window.location.href,
      isImageTrackingShown: false,
      isFaceFilterShown: false,
      isWAStickerShown: false,
      userLocation: {
        lat: 0,
        lng: 0,
      },
      marker1: {
        location: {
          lat: 22.2838265,
          lng: 114.1718732,
        },
        isCloser: false,
        type: "marker1",
        typeMarker: "pumpkin",
      },
      marker2: {
        location: {
          lat: 22.2826287,
          lng: 114.1705257,
        },
        isCloser: false,
        type: "marker2",
        typeMarker: "wa",
      },
      marker3: {
        location: {
          lat: -7.6693126,
          lng: 112.9133644,
        },
        isCloser: false,
        type: "marker3",
        typeMarker: "pumpkin",
      },
      marker4: {
        location: {
          lat: 22.28275,
          lng: 114.1697693,
        },
        isCloser: false,
        type: "marker4",
        typeMarker: "pumpkin",
      },
      marker5: {
        location: {
          lat: 22.282839,
          lng: 114.1683692,
        },
        isCloser: false,
        type: "marker5",
        typeMarker: "wa",
      },
      marker6: {
        location: {
          lat: 22.2832268,
          lng: 114.1674301,
        },
        isCloser: false,
        type: "marker6",
        typeMarker: "pumpkin",
      },
      marker7: {
        location: {
          lat: 22.283022,
          lng: 114.167677,
        },
        isCloser: false,
        type: "marker7",
        typeMarker: "fake",
      },
      marker8: {
        location: {
          lat: 22.282923,
          lng: 114.167897,
        },
        isCloser: false,
        type: "marker8",
        typeMarker: "fake",
      },
      marker9: {
        location: {
          lat: 22.282849,
          lng: 114.168289,
        },
        isCloser: false,
        type: "marker9",
        typeMarker: "fake",
      },
      marker10: {
        location: {
          lat: 22.282804,
          lng: 114.168552,
        },
        isCloser: false,
        type: "marker10",
        typeMarker: "fake",
      },
      marker11: {
        location: {
          lat: 22.282794,
          lng: 114.168744,
        },
        isCloser: false,
        type: "marker11",
        typeMarker: "fake",
      },
      marker12: {
        location: {
          lat: 22.2827512,
          lng: 114.1690582,
        },
        isCloser: false,
        type: "marker12",
        typeMarker: "fake",
      },
      marker13: {
        location: {
          lat: 22.2827606,
          lng: 114.1692681,
        },
        isCloser: false,
        type: "marker13",
        typeMarker: "fake",
      },
      marker14: {
        location: {
          lat: 22.2827311,
          lng: 114.1694676,
        },
        isCloser: false,
        type: "marker14",
        typeMarker: "fake",
      },
      marker15: {
        location: {
          lat: 22.2826746,
          lng: 114.1696811,
        },
        isCloser: false,
        type: "marker15",
        typeMarker: "fake",
      },
      marker16: {
        location: {
          lat: 22.2826498,
          lng: 114.1699065,
        },
        isCloser: false,
        type: "marker16",
        typeMarker: "fake",
      },
      marker17: {
        location: {
          lat: 22.2826,
          lng: 114.17015,
        },
        isCloser: false,
        type: "marker17",
        typeMarker: "fake",
      },
      marker18: {
        location: {
          lat: 22.2825701,
          lng: 114.1704177,
        },
        isCloser: false,
        type: "marker18",
        typeMarker: "fake",
      },
      marker19: {
        location: {
          lat: 22.2826591,
          lng: 114.1706518,
        },
        isCloser: false,
        type: "marker19",
        typeMarker: "fake",
      },
      marker20: {
        location: {
          lat: 22.2827596,
          lng: 114.1708811,
        },
        isCloser: false,
        type: "marker20",
        typeMarker: "fake",
      },
      marker21: {
        location: {
          lat: 22.2828794,
          lng: 114.1710782,
        },
        isCloser: false,
        type: "marker21",
        typeMarker: "fake",
      },
      marker22: {
        location: {
          lat: 22.2830528,
          lng: 114.171266,
        },
        isCloser: false,
        type: "marker22",
        typeMarker: "fake",
      },
      marker23: {
        location: {
          lat: 22.2834127,
          lng: 114.1714326,
        },
        isCloser: false,
        type: "marker23",
        typeMarker: "fake",
      },
      closeMarkers: [],
      closestMarker: 0,
      chosenMarker: 0,
      chosenCharacter: null,
      imageStickerTracked: 0,
      imageFaceFilterTracked: 0,
      stickerChosen: null,
    };
  }

  async componentDidMount() {
    ReactGA.pageview("/");
    if(isIOS && !isSafari){
      window.open('x-web-search://?ar', "_self");
      window.location.href = this.state.locationURL;
    } else if (isAndroid && !isChrome) {
			//if it's on android, but not using chrome then we will open chrome
			window.location.href = 'googlechrome://navigate?url=' + this.state.locationURL;
		} else if( isAndroid && isChrome || isIOS && isSafari) {
      const updatePosition = (position) => {
        this.setState(
          {
            userLocation: {
              lat: position.coords.latitude,
              lng: position.coords.longitude,
            },
          },
          () => {
            this.showPosition();
          }
        );
      };
      if (navigator.geolocation) {
        this.geolocationWatcher = navigator.geolocation.watchPosition(
          updatePosition,
          () => {
            alert("Geolocation not available.");
          },
          {
            enableHighAccuracy: true,
            maximumAge: 0,
          }
        );
      } else {
        alert("Your browser does not support geolocation.");
      }
    }
  }

  componentWillUnmount() {
    // navigator.geolocation.clearWatch(this.geolocationWatcher);
  }

  showPosition = () => {
    const { userLocation } = this.state;
    var { closestMarker, closeMarkers } = this.state;

    var markers = {
      marker1: this.state.marker1,
      marker2: this.state.marker2,
      marker3: this.state.marker3,
      marker4: this.state.marker4,
      marker5: this.state.marker5,
      marker6: this.state.marker6,
      marker7: this.state.marker7,
      marker8: this.state.marker8,
      marker9: this.state.marker9,
      marker10: this.state.marker10,
      marker11: this.state.marker11,
      marker12: this.state.marker12,
      marker13: this.state.marker13,
      marker14: this.state.marker14,
      marker15: this.state.marker15,
      marker16: this.state.marker16,
      marker17: this.state.marker17,
      marker18: this.state.marker18,
      marker19: this.state.marker19,
      marker20: this.state.marker20,
      marker21: this.state.marker21,
      marker22: this.state.marker22,
      marker23: this.state.marker23,
    };

    var distanceToMarkerAll = [];
    closeMarkers = [];

    for (var i = 0; i < 23; i++) {
      let markerIndex = i + 1;
      let marker = markers[`marker${markerIndex}`];
      if (!marker) {
        continue; //Skip for not exist marker
      }
      let distanceToMarker = haversine(
        { lng: marker.location.lng, lat: marker.location.lat },
        userLocation
      );

      distanceToMarkerAll[i] = distanceToMarker;

      if (distanceToMarker <= 30) {
        //less than 25 meters
        markers[`marker${markerIndex}`].isCloser = true;
        markers[`marker${markerIndex}`].type = `marker${markerIndex}`;
        closeMarkers[i] = `marker${markerIndex}`;
      }
    }

    distanceToMarkerAll[0] = 1000; // temporary because marker1 is not fixed yet
    var closestDistance = Math.min(...distanceToMarkerAll);
    var closestMarkerIndex = distanceToMarkerAll.indexOf(closestDistance);
    closestMarker = closestMarkerIndex + 1;

    this.setState({
      marker1: markers.marker1,
      marker2: markers.marker2,
      marker3: markers.marker3,
      marker4: markers.marker4,
      marker5: markers.marker5,
      marker6: markers.marker6,
      marker7: markers.marker7,
      marker8: markers.marker8,
      marker9: markers.marker9,
      marker10: markers.marker10,
      marker11: markers.marker11,
      marker12: markers.marker12,
      marker13: markers.marker13,
      marker14: markers.marker14,
      marker15: markers.marker15,
      marker16: markers.marker16,
      marker17: markers.marker17,
      marker18: markers.marker18,
      marker19: markers.marker19,
      marker20: markers.marker20,
      marker21: markers.marker21,
      marker22: markers.marker22,
      marker23: markers.marker23,
      closeMarkers,
      closestMarker,
    });
  };

  handleCheckboxSticker = (character) => {
    ReactGA.event({
      category: "User",
      action: "Show Sticker",
      value: character,
    });
    this.setState({
      isImageTrackingShown: false,
      chosenCharacter: character,
      isWAStickerShown: true,
    });
  };

  handleCheckboxFaceFilter = (num) => {
    ReactGA.event({
      category: "User",
      action: "Show Face Filter",
      value: num,
    });
    this.setState({
      isImageTrackingShown: false,
      imageFaceFilterTracked: num,
      isFaceFilterShown: true,
    });
  };

  handleTriggerImageTracking = (type) => {
    ReactGA.event({
      category: "User",
      action: "Show Image Tracking",
      value: type,
    });
    this.setState({
      isImageTrackingShown: true,
      chosenMarker: type,
    });
  };

  backToImageTracking = () => {
    this.setState({
      isImageTrackingShown: true,
      isWAStickerShown: false,
    });
  };

  handleBackToHomepage = () => {
    this.setState({
      isImageTrackingShown: false,
      isWAStickerShown: false,
      isFaceFilterShown: false,
      chosenMarker: 0,
      chosenCharacter: null,
    });
  };

  handleCopyLink = () => {
    copy(this.state.locationURL);
    alert("copied!")
  }

  render() {
    const {
      isFaceFilterShown,
      isWAStickerShown,
      isImageTrackingShown,
      imageFaceFilterTracked,
      closeMarkers,
      chosenCharacter,
      chosenMarker,
      closestMarker,
    } = this.state;

    let content;

    if (isFaceFilterShown) {
      switch (imageFaceFilterTracked) {
        case "marker1":
          content = <FaceFilterBat />;
          break;
        case "marker3":
          content = <FaceFilterWitch />;
          break;
        case "marker4":
          content = <FaceFilterPumpkin />;
          break;
        case "marker6":
          content = <FaceFilterDevil />;
          break;
      }
    } else if (isWAStickerShown) {
      content = (
        <WASticker
          backToImageTracking={this.backToImageTracking}
          character={chosenCharacter}
        />
      );
    } else if (isImageTrackingShown) {
      let footer = null;
      if (["marker2", "marker5"].includes(chosenMarker)) {
        footer = (
          <div className="footer">
            把鏡頭對準萬聖嘩鬼!
            <br />
            Aim your camera at the spirit
          </div>
        );
      } else {
        footer = (
          <div className="footer" style={{ fontSize: "12px" }}>
            把鏡頭對準維港電視，啟動AR嘩鬼相機!
            <br />
            Aim your camera at the Victoria Harbour TV to activeate the
            Halloween AR Camera Filter.
          </div>
        );
      }
      content = (
        <div>
          <TeachableMachineTracking
            markerType={this.state.chosenMarker}
            triggerFaceFilter={(marker) =>
              this.handleCheckboxFaceFilter(marker)
            }
            triggerWASticker={(marker) => this.handleCheckboxSticker(marker)}
          />
          {footer}
          <img className="camera-frame" alt="Camera frame" src={frame} />
        </div>
      );
    } else {
      let pumpkinUser = null;
      let pumpkinUserStyle = null;

      if (closeMarkers.length > 0) {
        switch (closestMarker) {
          case 1:
            pumpkinUserStyle = style.repositionUserToMarker1;
            break;
          case 2:
            pumpkinUserStyle = style.repositionUserToMarker2;
            break;
          case 3:
            pumpkinUserStyle = style.repositionUserToMarker3;
            break;
          case 4:
            pumpkinUserStyle = style.repositionUserToMarker4;
            break;
          case 5:
            pumpkinUserStyle = style.repositionUserToMarker5;
            break;
          case 6:
            pumpkinUserStyle = style.repositionUserToMarker6;
            break;
          case 7:
            pumpkinUserStyle = style.repositionUserToMarker7;
            break;
          case 8:
            pumpkinUserStyle = style.repositionUserToMarker8;
            break;
          case 9:
            pumpkinUserStyle = style.repositionUserToMarker9;
            break;
          case 10:
            pumpkinUserStyle = style.repositionUserToMarker10;
            break;
          case 11:
            pumpkinUserStyle = style.repositionUserToMarker11;
            break;
          case 12:
            pumpkinUserStyle = style.repositionUserToMarker12;
            break;
          case 13:
            pumpkinUserStyle = style.repositionUserToMarker13;
            break;
          case 14:
            pumpkinUserStyle = style.repositionUserToMarker14;
            break;
          case 15:
            pumpkinUserStyle = style.repositionUserToMarker15;
            break;
          case 16:
            pumpkinUserStyle = style.repositionUserToMarker16;
            break;
          case 17:
            pumpkinUserStyle = style.repositionUserToMarker17;
            break;
          case 18:
            pumpkinUserStyle = style.repositionUserToMarker18;
            break;
          case 19:
            pumpkinUserStyle = style.repositionUserToMarker19;
            break;
          case 20:
            pumpkinUserStyle = style.repositionUserToMarker20;
            break;
          case 21:
            pumpkinUserStyle = style.repositionUserToMarker21;
            break;
          case 22:
            pumpkinUserStyle = style.repositionUserToMarker22;
            break;
          case 23:
            pumpkinUserStyle = style.repositionUserToMarker23;
            break;
        }
        pumpkinUser = (
          <img
            className="pumpkin-user"
            src={pumpkinInfo}
            style={pumpkinUserStyle}
          />
        );
      } else {
        pumpkinUser = (
          <div className="initial-info">
            <img className="pumpkin-info" src={pumpkinInfo} />
            <div className="text-info blurred">
              請跟著南瓜人的腳步一起尋找AR萬聖嘩鬼!
              <br />
              Please follow me and search for the spooky but cute spirits
              together!
            </div>
          </div>
        );
      }

      content = (
        <div className="map-container bg-map">
          {pumpkinUser}

          <div className="marker-container">
            {this.state.closeMarkers.map((marker, index) => (
              <div key={index}>
                <button
                  onClick={() => this.handleTriggerImageTracking(marker)}
                  className={"icon-btn " + marker}
                >
                  <img
                    src={
                      this.state[marker].typeMarker === "pumpkin"
                        ? pumpkinIcon
                        : waIcon
                    }
                    className="marker"
                  />
                </button>
              </div>
            ))}
          </div>
        </div>
      );
    }

    return (
      isIOS && isSafari || 
      isAndroid && isChrome ? 
        <div className="container">
          <img
            onClick={this.handleBackToHomepage}
            className="top-bar"
            src={topBanner}
            alt="top bar"
          />
          <div className="content">
            {content}
          </div>
        </div>
      :
        <div className="prompt-container">
          <img className="browser-logo" src={isIOS ? safariLogo : chromeLogo} />
          <div>
            <p className="my-3">
              {isIOS ? "Open with Safari for iOS to access this content" : "Open with Chrome for Android to access this content"}
            </p>
            <p className="mb-2">Tap below to copy the address for easy pasting into {isIOS ? "Safari for iOS" : "Chrome for Android"}</p>

            <div className="copy-link-container">
              <input type="text" value={this.state.locationURL} readOnly id="copy-input-link"/>
              <button onClick={this.handleCopyLink} className="btn copy-button">COPY</button>
            </div>
          </div>
        </div>
    );
  }
}

export default App;
